# README

#### 编译

```bash
bash build.sh
```



#### 使用

```bash
bash run.sh `文件路径`
```

例如：

```bash
#简单的例子
sh run.sh example.aig
#增加了计时和复杂路径的例子
time sh run.sh ../hisilicon/new_test/test2.aig
```



其中，example.aig已经位于目录下

- 若无解，即等价，则会显示s UNSATISFIABLE。

  ```
  [eda220322@ecs-71d9 ~]$ time sh run.sh example.aiger
  正在求解 example.aiger
  s UNSATISFIABLE
  
  real    0m0.099s
  user    0m0.096s
  sys     0m0.003s
  ```

- 若有解，即不等价，则会输出特解，例如,

  ```
  [eda220322@ecs-71d9 ~]$ time sh run.sh ../hisilicon/new_test/test2.aig
  正在求解 ../hisilicon/new_test/test2.aig
  s SATISFIABLE v -1 2 3 -4 -5 -6 -7 -8 -9 -10 -11 -12 -13 -14 -15 16 -17 -18 19  -40 -41 -42 v 43 -44 -45 46 -47 -48 -49 -50 -51 -52 -53 -54 55 56 -57 -58 -59 9 -80 -81 82 v -83 -84 -85 86 87 -88 89 -90 91 -92 -93 -94 -95 96 97 98 99 -1006 -117 -118 119 v 120 121 122 123 124 -125 -126 -127 -128 -129 -130 -131 -132 -8 -149 -150 -151 v -152 153 154 -155 -156 -157 -158 -159 -160 -161 162 -163 16480 181 182 183 v 184 185 186 -187 -188 -189 -190 191 ...
  ```